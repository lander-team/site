using PlotlyJS

using CSV
using DataFrames
using NumericalIntegration



thrustcurve = """
t F
0.148 7.638
0.228 12.253
0.294 16.391
0.353 20.210
0.382 22.756
0.419 25.260
0.477 23.074
0.520 20.845
0.593 19.093
0.688 17.500
0.855 16.225
1.037 15.427
1.205 14.948
1.423 14.627
1.452 15.741
1.503 14.785
1.736 14.623
1.955 14.303
2.210 14.141
2.494 13.819
2.763 13.338
3.120 13.334
3.382 13.013
3.404 9.352
3.418 4.895
3.450 0.000
"""

tc = CSV.read(IOBuffer(thrustcurve), DataFrame)

tcimp = integrate(tc.t, tc.F) |> x -> round(x, digits = 2)


begin
    df = CSV.read(".\\simOut_29.csv", DataFrame)


    df = df[180:end-100, :]
    df.t = (df.t .- 895) ./ 1000
    df.az = df.az .+ 9.81 .+ 1.2

    dfimp = integrate(df.t, df.az) |> x -> round(x, digits = 2)
end

# p = let
# lines(df.t, df.az)
# plot!((:t .- 895) ./ 1000, :az .+ 9.81 .+ 1.2, label = "Load Cell", linewidth = 3)
# @df tc plot!(:t, :F, label = "Expected", linewidth = 3)
# title!("Operational Demonstration Data")
# plot!(ylabel = "Thrust (N)", xlabel = "Time (s)", legend = :top, background = :transparent)

# end

# let
#     lines(df.t, df.az, color = :red, label = "sin")
#     lines!(tc.t, tc.F, color = :blue, label = "cos")
#     axislegend()
#     current_figure()
# end

p = let
    p1 = scatter(x = df.t, y = df.az, name = "Collected Data, $dfimp (Ns)")
    p2 = scatter(x = tc.t, y = tc.F, name = "Expected Data, $tcimp (Ns)")

    layout = Layout(
        # title = "Operational Demonstration Data",
        xaxis_title = "Time (s)",
        yaxis_title = "Force (N)",
        legend = attr(
            x = 1,
            y = 1.02,
            yanchor = "bottom",
            xanchor = "right",
            orientation = "h"
        ),
    )

    plot([p1, p2], layout)
end

open("./opdem.html", "w") do io
    PlotlyBase.to_html(io, p.plot,)
end